import React from 'react';
const Model =({selectImg,setSelectImg})=>{
    const handleclick=(e)=>{

        if(e.target.classList.contains('backdrop')){
            setSelectImg(null);
        }
    }
    return(
        <div className="backdrop" onClick={handleclick}>
            <img src={selectImg} alt="pic"/>
        </div>
    )

}
export default Model