import React from 'react';
import useFirestore from '../hooks/useFirstore';


const ImageGrid = ({setSelectImg}) => {
  const { docs } = useFirestore('images');
// const ImageGrid=()=>{
//     const { docs }=useFireStore('images');
    console.log(docs);
    return(
        <div className="img-grid">
            {docs && docs.map(doc=>(
                <div className="img-wrap" key={doc.id}
                onClick={()=> setSelectImg(doc.url)}
                >
                    <img src={doc.url} alt="upload"/>
                </div>
            ))}
        </div>
    )
}
export default ImageGrid;