import * as firebase from 'firebase/app';
import 'firebase/storage';
import 'firebase/firestore';
var firebaseConfig = {
    apiKey: "AIzaSyCfCm4y7ySlri8RJ4S0WsWBIuOguFYy0yM",
    authDomain: "gallery-e0563.firebaseapp.com",
    databaseURL: "https://gallery-e0563.firebaseio.com",
    projectId: "gallery-e0563",
    storageBucket: "gallery-e0563.appspot.com",
    messagingSenderId: "997626075681",
    appId: "1:997626075681:web:1175e68fc07c4dddc8e160"
  };
  // Initialize Firebase
  firebase.initializeApp(firebaseConfig);

  const projectStorage =  firebase.storage();
  const projectFirestore = firebase.firestore();
  const timestamp=firebase.firestore.FieldValue.serverTimestamp;

  export { projectFirestore,projectStorage ,timestamp};