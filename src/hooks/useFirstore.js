import {useState ,useEffect} from 'react';
import {projectFirestore} from '../firebase/cofig';
const useFirestore=(collection)=>{
    const[docs,setDocs]=useState([]);

    useEffect(()=>{
       const ClT= projectFirestore.collection(collection)
        .orderBy('createAt','desc')
        .onSnapshot((snap)=>{
            let documents=[];
            snap.forEach(doc=>{
                documents.push({...doc.data(),id:doc.id})
            });
            setDocs(documents);

        });
            return ()=>ClT();

    },[collection])
    
    return{docs};
}
export default useFirestore